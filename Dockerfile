FROM golang:1.17

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY main.go ./
COPY .env ./

RUN go build -o /mini-app-mas-naufal

CMD [ "/mini-app-mas-naufal" ]