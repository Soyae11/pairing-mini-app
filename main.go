package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"github.com/joho/godotenv"
)

type User struct {
	Id     string `json:"id"`
	Email  string `json:"email"`
	Name   string `json:"name"`
	Locale string `json:"locale"`
}

var (
	oauthConfigGoogle = &oauth2.Config{
		ClientID:     "",
		ClientSecret: "",
		RedirectURL:  "https://mini-app-pairing.herokuapp.com/",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	oauthState      = ""
	LOGIN			= "https://mini-app-pairing.herokuapp.com/login"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	oauthState = os.Getenv("OAUTH_STATE")
	oauthConfigGoogle.ClientSecret = os.Getenv("CLIENT_SECRET")
	oauthConfigGoogle.ClientID = os.Getenv("CLIENT_ID")

	r := gin.Default()         
	r.GET("/login", func(c *gin.Context) {
		URL, _ := url.Parse(oauthConfigGoogle.Endpoint.AuthURL)

		parameters := url.Values{}
		parameters.Add("client_id", oauthConfigGoogle.ClientID)
		parameters.Add("scope", strings.Join(oauthConfigGoogle.Scopes, " "))
		parameters.Add("redirect_uri", oauthConfigGoogle.RedirectURL)
		parameters.Add("response_type", "code")
		parameters.Add("state", oauthState)

		URL.RawQuery = parameters.Encode()
		url := URL.String()
		c.Redirect(http.StatusTemporaryRedirect, url)
	})
	r.GET("/", func(c *gin.Context) {
		if c.Query("code") == "" {
			c.AbortWithStatusJSON(http.StatusInternalServerError , gin.H{"status": false, "message": "You have to login first", "login_endpoint": LOGIN})
			return
		}
		if c.Query("state") != oauthState {
			c.AbortWithStatusJSON(http.StatusInternalServerError , gin.H{"status": false, "message": "My endpoint, my rule. Please relogin", "login_endpoint": LOGIN})
			return
		}
		token, err := oauthConfigGoogle.Exchange(oauth2.NoContext, c.Query("code"))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError , gin.H{"status": false, "message": err.Error(), "login_endpoint": LOGIN})
			return
		}
		res, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError , gin.H{"status": false, "message": err})
			return
		}
		var user User
		response, _ := ioutil.ReadAll(res.Body)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError , gin.H{"status": false, "message": err})
			return
		}
		defer res.Body.Close()	
		json.Unmarshal(response, &user)
		c.JSON(200, gin.H{
			"message": user,
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
